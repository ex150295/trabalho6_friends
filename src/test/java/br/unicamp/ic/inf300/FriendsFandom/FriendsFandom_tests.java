package br.unicamp.ic.inf300.FriendsFandom;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class FriendsFandomTest{
    static FriendsFandom fandom;

    @BeforeAll
    public static void init(){
        fandom = new FriendsFandom();
    }

    @Test
    @DisplayName("Season number should be validated by the database")
    public void test_validateSeason(){
        assertEquals(true,fandom.validateSeason(1));
    }
    @Test
    @DisplayName("Episodes should be fetch from database if season number is valid")
    public void test_fetchEpisodesFromSeason(){
        assertEquals(false,fandom.episodesFromSeason(4).isEmpty());
    }
    @Test
    @DisplayName("Invalid season number should return empty episode names list")
    public void test_invalidSeasonShouldReturnEmpty(){
        assertEquals(true,fandom.episodesFromSeason(30).isEmpty());
    }
    @Test
    @DisplayName("Invalid season number should return 0 episode count")
    public void test_invalidSeasonShouldCountZeroEpisodes(){
        assertEquals(0, fandom.countOfEpisodesInSeason(30));
    }
    @Test
    @DisplayName("Valid season number should not reutnr 0 episode count")
    public void test_validSeasonShouldNotCoundZeroEpisodes(){
        assertEquals(false, 0==fandom.countOfEpisodesInSeason(5));
    }
    @Test
    @DisplayName("Last season should not be 0")
    public void test_lastSeasonShouldNotBe0(){
        assertEquals(true, 0!=fandom.lastSeason());
    }
}

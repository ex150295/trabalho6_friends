package br.unicamp.ic.inf300.FriendsFandom;

import java.util.Scanner;
import java.util.Vector;

public class App{
    public static void main (String[] args){
        int seasonNo;
        FriendsFandom fandom = new FriendsFandom();
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("::: Friends Fandom :::");
            System.out.println("Tell me from which season you want to see the list of episodes. Or type 0 to exit:");
            seasonNo = sc.nextInt();
            if(seasonNo==0){
                System.out.println("\t === Good bye!!! ===");
                continue;
            }
            if(!fandom.validateSeason(seasonNo)){
                System.out.println("Invalid season typed. Starting from 1 the last season was " + fandom.lastSeason());
                continue;
            }
            Vector<String> episodes = fandom.episodesFromSeason(seasonNo);

            System.out.println("The list of episode names is listed below:");

            for (String episode : episodes){
                System.out.println("\t" + episode);
            }

        } while (seasonNo!=0);
        sc.close();
    }
}

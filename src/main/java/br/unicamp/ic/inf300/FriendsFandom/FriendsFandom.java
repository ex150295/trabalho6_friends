package br.unicamp.ic.inf300.FriendsFandom;

import java.util.Vector;
import java.util.Collections;
import java.util.logging.Level;
import com.mongodb.client.MongoCollection;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import java.util.logging.Logger;
import com.mongodb.MongoClientURI;
import com.mongodb.client.DistinctIterable;
import org.bson.Document;

class FriendsFandom{
    private final MongoClient client;
    private final MongoDatabase db;
    private final MongoCollection<Document> seasonData;
    private final Vector<Integer> seasons;

    public  FriendsFandom(){
        Logger mongoLogger = Logger.getLogger("org.mongodb.driver");
        mongoLogger.setLevel(Level.SEVERE);
        client = new MongoClient(new MongoClientURI("mongodb://127.0.0.1:27017"));
        db = client.getDatabase("friends");
        seasonData = db.getCollection("samples_friends");
        DistinctIterable<Integer> docs = seasonData.distinct("season",Integer.class);
        seasons = new Vector<Integer>();
        docs.into(seasons);
        Collections.sort(seasons);
    }
    public boolean validateSeason(int seasonNo){
        return seasons.contains(seasonNo);
    }

    public Vector<String> episodesFromSeason(int seasonNo){
        Vector<String> episodes = new Vector<String>();

        DistinctIterable<String> eps = seasonData.distinct("name",new Document("season", seasonNo),String.class);

        eps.into(episodes);

        return episodes;
    }

    public long countOfEpisodesInSeason(int seasonNo){
        return seasonData.countDocuments(new Document("season", seasonNo));
    }

    public int lastSeason(){
        return seasons.lastElement();
    }
}
